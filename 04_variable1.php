<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $nombre="Miguel";
            $nombres=["Eva","Jose","Ana", "Luis"];
        ?>
        
        <div>
            <?= $nombre ?>
        </div>
        
        <?php
        //Si pongo variable dentro de comillas dobles entiende que es el valor de la variable, no el texto
        //Si pongo comillas simples entiende que es texto
        foreach($nombres as $k=>$v){
            //Con comillas dobles
            echo "<div>$k- $v </div>";
            //Con comillas simples
            echo '<div>' . $k . '-' . $v . '</div>';
        }
        ?>
    </body>
</html>
